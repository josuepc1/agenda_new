<?php
// Conexão
require_once 'db_connect.php';

// Session
session_start();

// Verificación
if(!isset($_SESSION['logado'])):
	header('Location: index.php');
endif;

// Dados
$id = $_SESSION['id_usuario'];
$sql = "SELECT * FROM tb_admin WHERE id_usuario = '$id'";
$resultado = mysqli_query($connect, $sql);
$dados = mysqli_fetch_array($resultado);
mysqli_close($connect);
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">
	<title>Página restringida</title>
</head>
<body>
<h1> Hola <?php echo $dados['nome']; ?> </h1>
<a href="logout.php">Sair</a>
</body>
</html>